function tipoSexo(sexo) {
  return (sexo === 'M') ? 'Masculino' : 'Feminino';
}

var sexoM = 'M';
console.log(tipoSexo(sexoM));

var sexoF = 'F';
console.log(tipoSexo(sexoF));
