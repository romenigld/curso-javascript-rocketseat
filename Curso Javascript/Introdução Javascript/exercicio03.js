function temHabilidade(skills) {
  const resultado = [];

  for(let i=0; i < skills.length; i++){
    if (skills[i] === "Javascript") {
      resultado.push(`index[${i}] with value ${skills[i]} is true`);
    } else {
      resultado.push(`index[${i}] with value ${skills[i]} is false`);
    }
  }
  return resultado;
}

var skills = ["VueJS", "ReactJS", "React Native", "Javascript"];

result = temHabilidade(skills)
console.log(result);
