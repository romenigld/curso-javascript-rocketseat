function experiencia(anos) {
  if ((anos == 0) || (anos <= 2)){
    return "Iniciante";
  } else if (anos == (2 || 3)){
    return "Intermediário";
  } else if ((anos == 4) || (anos == 5) || (anos == 6)){
    return "Avançado";
  } else if (anos >= 7){
    return "Jedi Master";
  } else {
    return "Nothing";
  }
}

var anosEstudo = 1.5;
result = experiencia(anosEstudo);
console.log(result);
