// IF
function retornaSexoIf(sexo) {
  if (sexo === 'M'){
    return 'Masculino';
  } else if(sexo === 'F') {
    return 'Feminino';
  } else {
    return 'Outro';
  }
}


  var resultado = retornaSexoIf('M');
  console.log(resultado);

  resultado = retornaSexoIf('F');
  console.log(resultado);

  resultado = retornaSexoIf('OhOIHH');
  console.log(resultado);

// Switch
function retornaSexoSwitch(sexo) {
  switch(sexo) {
    case 'M':
      return 'Masculino';
    case 'F':
      return 'Feminino';
    default:
      return 'Outro';
  }
}

var resultado2 = retornaSexoSwitch('M');
console.log(resultado2);
var resultado2 = retornaSexoSwitch('F');
console.log(resultado2);
var resultado2 = retornaSexoSwitch('Gay');
console.log(resultado2);
