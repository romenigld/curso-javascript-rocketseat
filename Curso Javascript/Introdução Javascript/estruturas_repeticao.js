// For
for (var i=0; i<= 15; i++){
  console.log(i);
}
// While
var j = 0;
while (j<= 10){
  console.log(j);
  j++;
}

// While 2
var j = 1287128731;
while (j > 50){
  console.log(j);
  j /= 5;
}
