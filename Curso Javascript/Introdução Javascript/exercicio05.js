var usuarios = [
  {
    nome: "Diego",
    habilidades: ["Javascript", "ReactJS", "Redux"]
  },
  {
    nome: "Gabriel",
    habilidades: ["VueJS", "Ruby on Rails", "Elixir"]
  }
];

function showMensagem(users){
  users.forEach(function(user){
    resultado = `O ${user.nome} possui habilidades: ${user.habilidades.join(", ")}.`
    console.log(resultado);
  });
}


showMensagem(usuarios);
